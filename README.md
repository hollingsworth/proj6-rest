# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Author
Revised by Sydney Hollingsworth, sholling@uoregon.edu
Credits to Michal Young and Ram Durairajan for the initial version of this code.

## Program Function
A calculator for ACP controle times. The algorithm used comes frmo Randonneurs USA, and is described here (https://rusa.org/pages/acp-brevet-control-times-calculator).
Built using AJAX in the frontend, and Flask in the backend, and packaged in a docker container.
Controle times are stored in a MongoDB database


## RESTful functionality


* RESTful service to exposes what is stored in MongoDB. Examples:
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* Representations are either CSV or JSON. Examples:
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Query parameter to get top "k" open and close times. Examples:

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

## Incomplete
