<html>
    <head>
        <title>CIS 322 REST-api: Brevet Controle Times</title>
    </head>

    <body>
        <h1>Open and Close Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
              $open_times = $obj->open_times;
              $close_times = $obj->close_times;
            if (is_array($open_times)){
                foreach ($open_times as $ot) {
                    echo "<li>$ot</li>";
                }
            }
            if (is_array($close_times)){
                foreach ($close_times as $ct) {
                    echo "<li>$ct</li>";
                }
            }
            ?>
        </ul>
    </body>
</html>
