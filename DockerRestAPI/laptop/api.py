# Laptop Service
import flask
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient
import logging
import os

logging.basicConfig()

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

def find_times():
    open_times = []
    close_times = []
    _items = db.tododb.find()
    for item in _items:
        open_times.append(str(item["open"]))
        close_times.append(str(item["close"]))
    return {"open_times": open_times, "close_times": close_times}

class listAll(Resource):
    def get(self):
        result = find_times()
        return result

class listOpenOnly(Resource):
    def get(self):
        result = find_times()
        result.pop('close_times')
        return result

class listCloseOnly(Resource):
    def get(self):
        result = find_times()
        result.pop('open_times')
        return result


class listAllCSV(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }
class listOpenCSV(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

class listCloseCSV(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }


# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listAllCSV, '/listAllCSV')
api.add_resource(listOpenCSV, '/listOpenCSV')
api.add_resource(listCloseCSV, '/listCloseCSV')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
